import { Router } from 'express';
import TableRepository from '../../services/repositories/table.repository';
const router = Router();

router.get('/', async(req, res) => {
    let tables = await TableRepository.getAll()
    res.json(tables)
})

export default router