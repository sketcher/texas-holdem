import express, { Router, Request, Response, NextFunction } from "express"
import { HttpException } from "../models/httpException";
import hand from "./hand/hand"
import root from "./root/root"
import table from "./table/table"

const routes = Router();

routes.use('/', root)
routes.use('/hand', hand)
routes.use('/table', table)

export default routes