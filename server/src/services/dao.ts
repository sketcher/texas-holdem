
import * as sqlite from 'sqlite3'
import { resolve } from 'path'

const sqlite3 = sqlite.verbose();

const DB_PATH = resolve(__dirname, '../../src/data/sqllite.db')
const db = new sqlite3.Database(DB_PATH, sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE)

/**
 * Data Access Object
 */
export default class DAO {

    static all(stmt: string, params = <any>[]) {
        return new Promise((res, rej) => {
            db.all(stmt, params, (err, result) => this.result(res, rej, err, result));
        })
    }
    static get(stmt: string, params = <any>[]) {
        return new Promise((res, rej) => {
            db.get(stmt, params, (err, result) => this.result(res, rej, err, result));
        })
    }

    static run(stmt: string, params = <any>[]) {
        return new Promise((res, rej) => {
            db.run(stmt, params, (err: Error | any, result: any) => this.result(res, rej, err, result));
        })
    }

    static result(resolve: Function, reject: Function, err: Error | any, res: any) {
        if (err) {
            console.error('Could not run', err)
            reject(err)
        } else {
            resolve(res)
        }
    }

    public static setupForDev() {
        db.serialize(() => {
            //   Drop Tables:
            const dropTablesTable = "DROP TABLE IF EXISTS tables";
            const dropItemsTable = "DROP TABLE IF EXISTS items";
            this.run(dropTablesTable);
            this.run(dropItemsTable);

            // Create Tables:
            const createTablesTable = "CREATE TABLE IF NOT EXISTS tables (id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT, state TEXT)";
            const createItemsTable = "CREATE TABLE IF NOT EXISTS items (id INTEGER PRIMARY KEY AUTOINCREMENT, value TEXT)";
            this.run(createTablesTable);
            this.run(createItemsTable);
            
            const psuedoState = {
                players:[],
                events:[]
            }

            const insertTables = `INSERT INTO tables (name, state) VALUES ('alpha_taböe', '${JSON.stringify(psuedoState)}');`
            // const insertItems = `INSERT INTO tables (name, price) VALUES ('book', 12.99), ('t-shirt', 15.99), ('milk', 3.99);`
            this.run(insertTables);
            // this.run(insertItems);
        });
    }

    private static dropAllTables(dao: DAO) {
        db.run(`SELECT name FROM sqlite_master WHERE type='table'`)
    }
}