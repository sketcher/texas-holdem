import Item from "../../models/item";
import BaseRepository from "./base.repository";

export default class ItemRepository extends BaseRepository<Item> {

}