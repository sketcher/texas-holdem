import DAO from "../dao";

// const db = new DAO('../../data/sqllite.db')

export default abstract class BaseRepository<T> {
    add(item: T): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
    update(id: string, item: T): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
    delete(id: string): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
    find(item: T): Promise<T[]> {
        throw new Error("Method not implemented.");
    }
    findOne(id: string): Promise<T> {
        throw new Error("Method not implemented.");
    }
    // dao: DAO
    // repo : string

    // constructor(dao: DAO {
    //     this.dao = dao
    // }

    // async getAll(): Promise<any> {
    //     const items = await this.dao.all("SELECT * FROM ?", [])
    //     return items
    // }

    // async add(item : any){

    // }
}