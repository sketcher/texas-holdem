import Table from "../../models/table";
import dao from "../dao";

export default class TableRepository{

    static async getAll(): Promise<Table[]> {
        const tables = await dao.all("SELECT * FROM tables", [])
        return <Table[]>tables
    }

    static async getById(id: string): Promise<Table> {
        const table = await dao.get("SELECT * FROM tables WHERE id = ?", [id])
        return <Table>table;
    }

    static async create(table: Table): Promise<boolean> {
        const stmt = `INSERT INTO tables (name, state) VALUES (?,?);`
        try {
            await dao.run(stmt, [table.name, table.state]);
            return true;
        } catch (err) {
            console.error(err);
            return false;
        }

    }

    static async update(table: Table): Promise<boolean> {
        const stmt = `UPDATE tables SET name = ?, price= ? WHERE id = ?;`
        try {
            await dao.run(stmt, [table.name, table.state, table.id]);
            return true;
        } catch (err) {
            console.error(err);
            return false;
        }
    }

    static async deleteItem(tableId: number) {
        const stmt = `DELETE FROM tables WHERE id = ?;`
        try {
            await dao.run(stmt, [tableId]);
            return true;
        } catch (err) {
            console.error(err);
            return false;
        }
    }
}