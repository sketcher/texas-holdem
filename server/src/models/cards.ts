import Express from 'express';

export interface Cards<T> extends Express.Request {
    body: T
}