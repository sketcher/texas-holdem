import express, { Express, Request, Response, ErrorRequestHandler, NextFunction } from 'express';
import dotenv from 'dotenv';
import routes from './routes/index'
import { HttpException } from './models/httpException';
import DAO from './services/dao';


dotenv.config();

const app: Express = express();
const port = process.env.PORT || 5000;

app.use('/', routes);
routes.use((req: Request, res: Response, next: NextFunction) => {
  res.status(404).send('404')
  // next(err)
})

// sql table setup
DAO.setupForDev()

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});