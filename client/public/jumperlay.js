var jumperlay = (function(){
    this.targets = []
    this.rectangles = []
    this.el = document.createElement('aside')

    const setoverlayStyle = (top = 0, left = 0, w = 0, h = 0) => {

        let style = {
            position:'absolute',
            top: `${top}px`,
            left: `${left}px`,
            width: `${w}px`,
            height: `${h}px`,
            backgroundColor: 'rgba(200, 50, 50, 0.73)'
        }
        Object.assign(this.el.style, style);
    }

    const update =()=>{
        const highestX = this.rectangles.map(x => x).sort((a,b) => a.left < b.left )
        const highestY = this.rectangles.map(x => x).sort((a,b) => a.top > b.top )
        console.log(this.rectangles, highestY)
        let x = highestX[0].left
        let y = highestY[0].top

        let combinedWidth = highestX[highestX.length - 1].right - highestX[0].left
        let combinedHeight = highestY[highestY.length - 1].bottom - highestY[0].top

        setoverlayStyle(y, x, combinedWidth, combinedHeight)
        document.body.append(this.el)
    }



    let self = {
        set:(...queries)=> {
            this.targets = queries.map(query => document.querySelector(query))
            this.rectangles = this.targets.map(target => target.getBoundingClientRect())
            update()
            console.log(this.el)
        }
    }

    return self
})();

jumperlay.set('#question-summary-55195261', '#question-summary-35328222')