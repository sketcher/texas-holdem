// @ts-check

// export interface EventMessage {
//     category?: EventMessageCategory,
//     // data: Object
// }

export enum EventMessageCategory {
    Unspecified = 0,
    Interaction = 1,
    Action = 2,
    PlayerAdded = 3,
    PlayeRemoved = 4,
}

/**
 * Replacement for the Vue 2-based EventBus.
 *
 * @template EventName
 */
class Bus {
    eventListeners: Map<any, any>
    constructor() {
        /**
         * @type {Map<EventName, Array<{ callback: Function, once: boolean }>>}
         */
        this.eventListeners = new Map()
    }

    /**
     * @param {EventMessageCategory} eventName
     * @param {Function} callback
     * @param {boolean} [once]
     * @private
     */
    registerEventListener(category: EventMessageCategory, callback: CallableFunction, once = false) {
        if (!this.eventListeners.has(category)) {
            this.eventListeners.set(category, [])
        }

        const eventListeners = this.eventListeners.get(category)
        eventListeners.push({ callback, once })
    }

    /**
     * See: https://v2.vuejs.org/v2/api/#vm-on
     *
     * @param {EventMessageCategory | EventMessageCategory[]} categoryOrCategories
     * @param {Function} callback
     */
    $on(categoryOrCategories: EventMessageCategory | EventMessageCategory[], callback: CallableFunction) {
        let arr: EventMessageCategory[] = [];

        arr = Array.isArray(categoryOrCategories) ? categoryOrCategories : [categoryOrCategories]

        arr.map(category => this.registerEventListener(category, callback))
    }

    /**
     * See: https://v2.vuejs.org/v2/api/#vm-once
     *
     * @param {EventMessageCategory} eventName
     * @param {Function} callback
     */
    $once(category: EventMessageCategory, callback: CallableFunction) {
        const once = true
        this.registerEventListener(category, callback, once)
    }

    /**
     * Removes all event listeners for the given event name or names.
     *
     * When provided with a callback function, removes only event listeners matching the provided function.
     *
     * See: https://v2.vuejs.org/v2/api/#vm-off
     *
     * @param {EventMessageCategory | EventMessageCategory[]} categoryOrCategories
     * @param {Function} [callback]
     */
    $off(categoryOrCategories: EventMessageCategory | EventMessageCategory[], callback = undefined) {
        const eventNames = Array.isArray(categoryOrCategories) ? categoryOrCategories : [categoryOrCategories]

        for (const eventName of eventNames) {
            const eventListeners = this.eventListeners.get(eventName)

            if (eventListeners === undefined) {
                continue
            }

            if (typeof callback === 'function') {
                for (let i = eventListeners.length - 1; i >= 0; i--) {
                    if (eventListeners[i].callback === callback) {
                        eventListeners.splice(i, 1)
                    }
                }
            } else {
                this.eventListeners.delete(eventName)
            }
        }
    }

    /**
     * See: https://v2.vuejs.org/v2/api/#vm-emit
     *
     * @param {EventMessageCategory} eventName
     * @param {any} args
     */
    $emit(category: EventMessageCategory | string, arg: any) {
        if (!this.eventListeners.has(category)) {
            return
        }

        const eventListeners = this.eventListeners.get(category)
        const eventListenerIndexesToDelete = []
        for (const [eventListenerIndex, eventListener] of eventListeners.entries()) {
            eventListener.callback(arg)

            if (eventListener.once) {
                eventListenerIndexesToDelete.push(eventListenerIndex)
            }
        }

        for (let i = eventListenerIndexesToDelete.length - 1; i >= 0; i--) {
            eventListeners.splice(eventListenerIndexesToDelete[i], 1)
        }
    }
}

const EventBus = new Bus()

export default EventBus
