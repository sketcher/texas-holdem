/**
 * @source https://javascript.info/fetch-progress#0d0g7tutne & https://github.com/AnthumChris/fetch-progress-indicators/blob/master/fetch-basic/supported-browser.js
 * @param {Function(number, number, object)} callback
 * @returns {Promise} result
 */
export const fetchBlob = (callback: Function) => async (uri: string, options?: RequestInit) : Promise<string> => {
    return new Promise<string>(async (resolve, reject) => {

        const response = await fetch(uri, options) as Response

        if (!response.ok) reject(response.status + ' ' + response.statusText);
        if (!response.body) reject('ReadableStream not yet supported in this browser.');

        const contentEncoding = response.headers.get('Content-Encoding');
        const contentLength = +(response.headers.get(contentEncoding ? 'x-file-size' : 'Content-Length') as unknown as string);

        if (contentLength === null) reject('Response size header unavailable')

        let loaded = 0, chunks = []

        callback({ loaded, contentLength, step: 0 })

        const reader = (response.body as ReadableStream<Uint8Array>).getReader()

        // step by step
        while (true) {
            const { done, value } = <{ done: Boolean, value: Uint8Array }>await reader.read()

            if (done) {
                break
            }

            callback({ loaded: value.length, contentLength, step: 1 })
            chunks.push(value)
            loaded += value.length
        }

        callback({ loaded, contentLength, step: 2 })

        // join up
        let chunksAll = new Uint8Array(loaded),
            position = 0

        for (let chunk of chunks) {
            chunksAll.set(chunk, position)
            position += chunk.length
        }

        // as string
        resolve(new TextDecoder("utf-8").decode(chunksAll))
    })
}