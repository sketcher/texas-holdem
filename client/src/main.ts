import './styles/root.css'
import './styles/theme.css'
import { createApp } from 'vue'
import App from './App.vue'
import common from './mixins/common'
import Deck from './models/deck';
import { getMessage, logCards } from './utils';


createApp({
    extends: App,
    mixins: [common]
}).mount('#app')



