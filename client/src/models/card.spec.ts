import Card, { CardType } from "./card"

describe('card', ()=>{
    it('to display correct symbols', () => {
        expect(CardType.cardFromString('A♠').display).toBe('A♠')
        expect(CardType.cardFromString('Q♥').display).toBe('Q♥')
    })
})
