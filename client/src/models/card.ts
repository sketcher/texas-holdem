const rank = [
    ...new Array(9).fill(2).map((x, i) => (x + i).toString()),
    ...['Kn', 'Q', 'K', 'A']
]

export const symbol = {
    spades: "♠",
    hearts: "♥",
    diamonds: "♦",
    clubs: "♣",
}

/**
 * suit = ♠♥♦♣
 * rank = 2,3,4..
 */
export default class Card {
    suit: string
    rank: string
    value: number
    id: number

    constructor(id: number) {
        this.id = id
        this.suit = Object.values(symbol)[Math.floor(id / 13)]
        this.value = id % 13
        this.rank = rank[this.value]
    }

    public get display(): string {
        return `${this.rank}${this.suit}`
    }

    public get color(): string {
        return this.suit === symbol.hearts || this.suit === symbol.diamonds ? 'red' : 'black'
    }

    public get displayGraphical(): string {
        const code = 56497;
        const unicodeSuffix = '\uD83C'
        const str = String.fromCharCode(code + this.id);
        return unicodeSuffix + str
    }
}

interface IObjectKeys {
    [key: string]: number;
}

export class CardType {
    private static types: IObjectKeys = {
        ['2♠']: 0, ['3♠']: 1, ['4♠']: 2, ['5♠']: 3, ['6♠']: 4, ['7♠']: 5, ['8♠']: 6, ['9♠']: 7, ['10♠']: 8, ['Kn♠']: 9, ['Q♠']: 10, ['K♠']: 11, ['A♠']: 12,
        ['2♥']: 13, ['3♥']: 14, ['4♥']: 15, ['5♥']: 16, ['6♥']: 17, ['7♥']: 18, ['8♥']: 19, ['9♥']: 20, ['10♥']: 21, ['Kn♥']: 22, ['Q♥']: 23, ['K♥']: 24, ['A♥']: 25,
        ['2♦']: 26, ['3♦']: 27, ['4♦']: 28, ['5♦']: 29, ['6♦']: 30, ['7♦']: 31, ['8♦']: 32, ['9♦']: 33, ['10♦']: 34, ['Kn♦']: 35, ['Q♦']: 36, ['K♦']: 37, ['A♦']: 38,
        ['2♣']: 39, ['3♣']: 40, ['4♣']: 41, ['5♣']: 42, ['6♣']: 43, ['7♣']: 44, ['8♣']: 45, ['9♣']: 46, ['10♣']: 47, ['Kn♣']: 48, ['Q♣']: 49, ['K♣']: 50, ['A♣']: 51
    }

    static idFromString(key: string): number {
        return this.types[key]
    }

    static cardFromString(key: string): Card {
        const [_rank, suit] = key.split('')

        // is reversed
        key = !rank.some(x => x === _rank) ? suit + rank : key

        return new Card(this.idFromString(key))
    }

    static cardFromValues(suit: string, rank: string): void {
        // todo;
    }
}