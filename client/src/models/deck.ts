import Card, { } from './card'
import CardCollection from './cardCollection'

export default class Deck extends CardCollection {
    cards: Card[] = []
    _disposedCards: Card[] = []

    constructor() {
        super()
        this.clear()
        this.reset()
    }

    reset() {
        for (let i = 0; i < 52; i++) {
            this.cards.push(new Card(i));
        }
    }
}