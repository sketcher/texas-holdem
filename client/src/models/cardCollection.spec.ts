import Card from "./card"
import Deck from "./deck"
import Hand from "./hand"

describe('model construction', () => {
  it('is creating card', () => {
    let card = new Card(12)
    expect(card.display).toBe('A♠')
  })

  it('is creating hand', () => {
    let hand = new Hand()
    expect(hand.cards.length).toBe(0)
  })

  it('is creating deck', () => {
    let deck = new Deck()
    expect(deck.cards.length).toBe(52)
  })
})

describe('model inheritance', () => {
  const deck = new Deck()
  const hand = new Hand()
  it('works', () => {
    let card = deck.pull()
    hand.add(card)

    expect(deck.cards.length).toBe(51)
    expect(hand.cards.length).toBe(1)
    expect(hand.pull().id === card.id).toEqual(true)
    expect(hand.cards.length).toBe(0)
  })
})
