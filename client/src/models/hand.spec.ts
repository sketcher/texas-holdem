import Card, { CardType } from "./card"
import Hand from "./hand"

const createCards = (...symbols: string[]) => symbols.map(x => CardType.cardFromString(x))

// "♠♥♦♣",
describe('hand', () => {
    const hand = new Hand()
    afterEach(() => hand.clear())

    describe('with 5 cards (2♠3♠4♠5♠6♠)', () => {
        // arrange & act
        const cards = createCards('2♠', '3♠', '4♠', '5♠', '6♠')
        hand.addMultiple(...cards)
        const combination = hand.getCombinations()
       
        // assert
        it('is straight', () => expect(combination.hasStraights).toBe(true))
        it('is flush', () => expect(combination.hasFlushes).toBe(true))
        it('does not have pair', () => expect(combination.hasPair).toBe(false))

    })
})

describe('multiple hands', () => {
    const hand = new Hand()
    beforeEach(() => hand.clear())
})