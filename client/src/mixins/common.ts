type IProgressBar = {
    start: Function
    step: Function
    stop: Function
    draw: Function
}

const ProgressBar = function (steps = 10) {
    const baseStyle = {
        ["font-size"]: `26em`,
        color: "whitesmoke",
    };
    const styleString = (obj: object) => Object.entries(obj)
        .map(([k, v]) => `${k}:${v}`)
        .join(";");

    const style = (obj: object) => styleString({ ...baseStyle, ...obj });

    const percentageOf = (a: number, b: number) => ~~((a / b) * 100);

    const barChar = String.fromCharCode(9616);
    let barFilled = new Array(steps).fill(barChar);
    let barEmpty = new Array(steps).fill(barChar);

    let total = 0;
    let currentStep = 0;
    let percentage = 0.0;
    let self: IProgressBar;

    self = {
        start: (length: number) => {
            console.clear();
            total = length;
            self.draw();
        },
        step: (len: number, length: number) => {
            if (length !== total) {
                total = length;
            }
            percentage = percentageOf(len, total);
            currentStep = Math.floor(percentage / steps);
            self.draw();
        },
        stop: () => {
            // do something?
        },
        draw: () => {
            const percentageString = String(`${percentage}%`).padEnd(4, " ");
            const pre = barFilled.slice(0, currentStep).join("");
            const post = barEmpty.slice(0, steps - currentStep).join("");
            console.log(
                `${percentageString}[%c${pre}%c${post}%c]`,
                styleString({
                    'color': 'hotpink'
                }),
                styleString({
                    'color': 'transparent'
                }),
                styleString({
                }),
            );
        },
    };

    return self;
}

export default {
    data() {
        return {
            ProgressBar: ProgressBar()
        }
    },
    created() {
        
    },
    methods: {

    },
}