<?php
$CURRENT_DIR = dirname(__FILE__);
['uri' => $uri] = parse_request();
[$files, $total] = get_files_in_directory($CURRENT_DIR);
header_cors();
send_json($files);
