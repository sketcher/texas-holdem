<?php

function get_files_in_directory($dir, $pattern = '/json$/')
{
    $files = [];
    $totalSize = 0;
    foreach (array_diff(scandir($dir), array('.', '..')) as $key => $val) {
        if (!preg_match($pattern, $val)) {
            continue;
        }
        $file_dir = $dir . DIRECTORY_SEPARATOR . $val;
        $propertyKey = explode('.', basename($file_dir))[0];
        $files[$propertyKey] = json_decode(file_get_contents($file_dir), true);
        $totalSize += filesize($file_dir);
    }
    return [$files, $totalSize];
}

function parse_request()
{
    if (!array_key_exists('REQUEST_URI', $_SERVER)) {
        echo 'No request uri. Bailing out';
        exit;
    }
    $uri = $_SERVER['REQUEST_URI'];

    return ['uri' => $uri];
}

function header_cors()
{

    // Allow from any origin
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
        // you want to allow, and if so:
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            // may also be using PUT, PATCH, HEAD etc
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        exit(0);
    }
}

function send_json($data)
{
    $content = json_encode($data);
    header("Connection: Keep-alive");
    header("Content-Type: application/json");
    $length = strlen($content);
    header("Content-Length: $length");
    echo $content;
}
