<?php
require_once(os_path_join(dirname(dirname(__FILE__)), 'helpers.php'));
require_once(os_path_join(dirname(dirname(__FILE__)), 'root.php'));

function os_path_join(...$parts)
{
    return preg_replace('#' . DIRECTORY_SEPARATOR . '+#', DIRECTORY_SEPARATOR, implode(DIRECTORY_SEPARATOR, array_filter($parts)));
}
