import Card from "./models/card";

export function getMessage(): string {
  return 'Hello world!';
}

export const logCards = (cards : Card[]) : void => {
  const str = cards.map(c => `%c${c.display}`).join('')
  const styleObj = {
      ['background-color']: 'white',
      padding: '1px 0.1px',
      margin: '4px',
      display: 'inline-flex',
      width: '36px',
      height: '48px',
      ['border-radius']: '2px',
      ['text-align']: 'center',
      ['font-size']: '20px',
      ['border-color']: '#000000',
      ['border-width']: '0.01em',
      ['border-style']: 'solid',

  }
  const styleStr = cards.map(c => `color:${c.color};${Object.entries(styleObj).map(([key, val]) => `${key}:${val};`).join('')}`)

  console.log(str, ...styleStr)
}