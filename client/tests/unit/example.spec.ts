import { shallowMount } from '@vue/test-utils'
import BoardComponent from '@/components/BoardComponent.vue'
import { guid } from '@/lib/random'

describe('BoardComponent.vue', () => {
  it('placeholder todo; remove', () => {
    expect(1).not.toEqual(2)
  })
  // this one below is ass slow
  // it('renders props.msg when passed', () => {
  //   const _guid = guid()
  //   const wrapper = shallowMount(BoardComponent, {
  //     props: { guid:_guid }
  //   })
  //   expect(wrapper.text()).toMatch(_guid)
  // })
})
