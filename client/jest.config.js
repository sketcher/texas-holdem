console.log('jest config setup')

module.exports = {
  preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',
  transform: {
    '^.+\\.vue$': 'vue-jest'
  },
  testMatch: [
    "<rootDir>/tests/**/*.(spec|test).{js,jsx,ts,tsx}",
    "<rootDir>/tests/**/?(*.)(spec|test).{js,jsx,ts,tsx}",
    "<rootDir>/src/**/*.(spec|test).{js,jsx,ts,tsx}",
  ],
  globals: {
    fetch: null
  }
}
