import Card from "./card";
import CardCollection from "./cardCollection";

export default class Hand extends CardCollection {
    constructor() {
        super()
    }

    public getCombinations() {
        const { has: hasPair, ids: pairs } = getPairs(this.cards)
        const { has: hasFlushes, ids: flushes } = getFlushes(this.cards)
        const { has: hasStraights, ids: straights } = getStraights(this.cards)

        return {
            hasPair,
            hasFlushes,
            hasStraights,
            pairs,
            flushes,
            straights,
        }
    }
}

function* incrementedChunks(arr: Card[], n: number) {
    let a = 0

    while (a < arr.length) {
        yield arr.slice(a, n)
        a++
        n++
    }
}

const isStraight = (cards: Card[]): boolean => {
    return cards.length >= 5 && cards.every((card, index) => card.value - cards[0].value === index)
}

const isFlush = (cards: Card[]): boolean => {
    return cards.length >= 5 && cards.every(card => card.suit === cards[0].suit)
}

export const getPairs = (cards: Card[]) => {
    let ids: number[][] = []
    const ranks = cards.map(x => x.rank)

    ranks.map((item, index) => {
        let _index = ranks.indexOf(item)
        if (_index != index) {
            ids.push([_index, index])
        }
    })

    return {
        has: ids.length > 0,
        ids
    }
}

export const getStraights = (cards: Card[]) => {
    let ids: number[][] = []
    let copy: Card[] = []
    Object.assign(copy, cards)

    copy.sort((left, right) => left.value < right.value ? 1 : 0)

    // chunks of five
    const chunks = [...incrementedChunks(copy, 5)]

    // not sure what I'm doing here
    chunks.filter(arr => isStraight(arr)).forEach((arr, index) => {
        ids[index] = [...arr.map(x => x.id)]
    })

    return {
        has: chunks.length > 0,
        ids
    }
}

export const getFlushes = (cards: Card[]) => {
    let ids: number[][] = []
    let copy: Card[] = []
    Object.assign(copy, cards)
    
    const chunks = [...incrementedChunks(copy, 5)]

    chunks.filter(arr => isFlush(arr)).forEach((arr, index) => {
        ids[index] = [...arr.map(x => x.id)]
    })

    return {
        has: chunks.length > 0,
        ids
    }
}