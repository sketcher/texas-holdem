import Player from "../player"
import ActionType from "./actionType"

export default class Action {
    public type : ActionType
    public player : Player

    constructor(player: Player, type: ActionType) {
        this.player = player
        this.type = type
    }
}
