enum ActionType {
    Nothing = 0,
    Fold = 1,
    Check = 2,
    Call = 3,
    Bet = 4,
    AllIn = 5
}
export default ActionType

export const allowedActions = ( types : ActionType[] ) => {
    return { 
        fold: types.includes(ActionType.Fold),
        check: types.includes(ActionType.Check),
        call: types.includes(ActionType.Call),
        bet: types.includes(ActionType.Bet),
        allIn: types.includes(ActionType.AllIn),
    }
}

export const types = () => {
    return [
        {  }
    ]
}