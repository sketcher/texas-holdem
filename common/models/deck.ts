import Card from "./card"
import CardCollection from "./cardCollection"

export default class Deck extends CardCollection {
    cards: Card[] = []
    _disposedCards: Card[] = []

    constructor() {
        super()
        this.clear()
        this.reset()
    }

    reset(): void {
        if(this.cards.length  === 0) {
            for (let i = 0; i < 52; i++) {
                this.cards.push(new Card(i));
            }
            return;
        }

        const len = this._disposedCards.length;
        while(len > 0) {
            this.cards.concat([this._disposedCards.shift()!])
        }
        console.log(this.cards.length)
        if(this.cards.length !== 52) {
            throw new Error('A complete deck must have 52 cards')
        }
    }
}