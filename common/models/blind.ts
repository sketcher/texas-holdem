export default class Blind {
    position: number = 0
    cash: number = 1
    type: string

    constructor(cash: number, type: string, position = 0) {
        this.cash = cash
        this.type = type
        this.position = position
    }

    advance(numberOfPlayers: number) {
        this.position = this.position >= numberOfPlayers ? 0 : this.position + 1
    }

    increaseBuyin(multiplier: number) {
        this.cash *= multiplier
    }

    static createBlinds(bigBlind: number, smallBlind: number) {
        return {
            bigBlind: this.bigBlind(bigBlind),
            smallBlind: this.smallBlind(smallBlind),
        }
    }

    static bigBlind(cash: number) {
        return new Blind(cash, 'big')
    }

    static smallBlind(cash: number) {
        return new Blind(cash, 'small')
    }
}