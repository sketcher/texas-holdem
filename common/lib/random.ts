import Card from "../models/card";

export const shuffle = (arr: Card[]) => {
    for (let i = arr.length - 1; i > 0; i--) {
        let rand = Math.floor(Math.random() * (i + 1));
        [arr[i], arr[rand]] = [arr[rand], arr[i]]
    }
}

export const getRandomIntInclusive = (min: number, max: number) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1) + min); //The maximum is inclusive and the minimum is inclusive
}

export const guid = (): string => {
    let result: string;
    let i: string;
    let j: number;

    result = "";
    for (j = 0; j < 32; j++) {
        if (j == 8 || j == 12 || j == 16 || j == 20)
            result = result + '-';
        i = Math.floor(Math.random() * 16).toString(16).toUpperCase();
        result = result + i;
    }
    return result
}

const playerNames = ["Deidra", "Khloe", "London", "Lyda", "Jonathon", "Morty", "Chad", "Clotilda", "Matthew", "Jensen", "BOBBY"]
export const randomName = (): string => playerNames[getRandomIntInclusive(0, playerNames.length - 1)]

export const randomCashSum = (): number => Math.round(getRandomIntInclusive(300, 600) / 100) * 150
