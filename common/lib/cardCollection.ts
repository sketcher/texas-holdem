
import { shuffle } from "../lib/random";
import Card from "../models/card";

export default class CardCollection {
    cards: Card[] = []

    constructor() {

    }

    add(card: Card): void {
        this.cards.push(card)
    }

    addMultiple(...cards: Card[]): void {
        this.cards.push(...cards)
    }

    clear(): void {
        this.cards = []
    }

    shuffle(): void {
        shuffle(this.cards)
    }

    /** Draw 1 card */
    pull(): Card {
        return this.cards.pop()!
    }

    /** Draw multiple cards */
    draw(n: number, dispose = 0): Card[] {
        const cards = []
        if (dispose > 0) {
            while (dispose-- > 0) {
                this.pull()
            }
        }

        while (n-- > 0) {
            cards.push(this.pull())
        }

        return cards
    }
}