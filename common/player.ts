import Hand from "./models/hand"
import { uuid } from "./lib/helper"
import { randomCashSum, randomName } from "./lib/random"

export default class Player {

    public guid: string
    public name: string = "unknown"
    public seat: number = -1
    public cash = 0
    public bet = 0
    public hand: Hand
    public isActive = true

    constructor(name: string) {
        this.guid = uuid()
        this.name = name
        this.hand = new Hand()
    }

    log(): object {
        const { name, cash, hand, isActive } = this
        return {
            name, cash, hand, isActive
        }
    }

    static createRandomPlayer(): Player {
        const rndPlayer = new Player(randomName())
        rndPlayer.cash = randomCashSum()
        return rndPlayer
    }
}