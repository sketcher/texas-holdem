import Action from "./models/action";
import Blind from "./models/blind";
import Deck from "./models/deck";
import Player from "./player";

enum GameStage {
    PreFlop = 0,
    Flop = 1,
    Turn = 2,
    River = 3,
    ShowDown = 4,
}

export default class Game {
    _round: number = 0          //
    _turn: number = 0           // track each player turn.
    _actionCounter: number = 0  // 
    _dealerButton: number = 0
    pot: number = 0
    players: Player[] = []
    logMessages: object[] = []
    stage: GameStage = GameStage.PreFlop
    bigBlind: Blind
    smallBlind: Blind
    deck: Deck

    constructor() {
        this.smallBlind = new Blind(10, 'small', 1)
        this.bigBlind = new Blind(25, 'big', 2)
        this.deck = new Deck()
    }

    get activePlayer(): Player {
        return this.players[this._turn]
    }

    public allowedActionsForActivePlayer(player: Player): any {
        if (player.cash <= 0 || !player.isActive) {
            return {}
        }
        if (player.seat === this.bigBlind.position || player.bet >= this.pot) {
            return {
                fold: true,
                check: true,
                bet: true,
                allIn: true
            }
        }

        return {
            fold: true,
            bet: true,
            allIn: true
        }
    }

    increase(n: number): number {
        return n >= this.players.length ? 0 : n + 1
    }

    newRound() {
        this._turn = 0
        this._actionCounter = 0

        // deal out cards
        this.deck.reset()
        this.deck.shuffle()
        this.deck.draw(0, 1)
        for (let activePlayer of this.players.filter(x => x.isActive)) {
            activePlayer.hand.addMultiple(...this.deck.draw(2, 0))
        }
    }

    // after each action
    afterActionCheck(action: Action) {
        if (this.stage == GameStage.PreFlop)
            this.preflop()
        else if (this.stage == GameStage.Flop)
            this.flop()
        else if (this.stage == GameStage.Turn)
            this.turn()
        else if (this.stage == GameStage.River)
            this.river()
        else if (this.stage == GameStage.ShowDown)
            this.showdown()
    }

    finish() {
        this._dealerButton = this.increase(this._dealerButton)
        this.smallBlind.position = this.increase(this.smallBlind.position)
        this.bigBlind.position = this.increase(this.bigBlind.position)
    }

    preflop() {
        const player = this.activePlayer
    }

    flop() {

    }

    turn() {

    }

    river() {

    }

    showdown() {

    }

    addPlayer(player: Player) {
        // ongoing game. sit out
        if (this._turn !== 0) {
            player.isActive = false
        }
        this.players.push(player)
        this.log(player, 'player_added')
    }

    log(obj: object, type: string = ''): void {
        const o = {
            type,
            ...obj
        }
        this.logMessages.push(o)
    }
}